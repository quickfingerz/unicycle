//
//  AppDelegate.m
//  VirtualStudio
//
//  Created by Tom Jackson on 19/03/2012.
//  Copyright (c) 2012 Quick Fingers. All rights reserved.
//

#import "AppDelegate.h"
#import <CoreServices/CoreServices.h>
@implementation AppDelegate

@synthesize window = _window;
@synthesize filesToOpenArray;
@synthesize configPlist = _configPlist;
@synthesize appleEventDescriptor = _appleEventDescriptor;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{}

- (void)application:(NSApplication *)sender openFiles:(NSArray *)filenames
{
	filesToOpenArray = [[NSMutableArray alloc] initWithArray:filenames];
	[filesToOpenArray sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    [self openAllTheseFiles:filesToOpenArray];
    filesToOpenArray = nil;
    NSAppleScript *run = [[NSAppleScript alloc] initWithSource:@"tell application \"Rider EAP\"\nactivate\nend tell"];
	[run executeAndReturnError:nil];
    [[NSApplication sharedApplication] terminate:NULL];
}

- (NSString *)resolveAliasInPath:(NSString *)path
{
	NSString *resolvedPath = nil;
	CFURLRef url = CFURLCreateWithFileSystemPath(NULL, (__bridge CFStringRef)path, kCFURLPOSIXPathStyle, NO);
	
	if (url != NULL) {
		FSRef fsRef;
		if (CFURLGetFSRef(url, &fsRef)) {
			Boolean targetIsFolder, wasAliased;
			if (FSResolveAliasFile (&fsRef, true, &targetIsFolder, &wasAliased) == noErr && wasAliased) {
				CFURLRef resolvedURL = CFURLCreateFromFSRef(NULL, &fsRef);
				if (resolvedURL != NULL) {
					resolvedPath = (__bridge NSString*)CFURLCopyFileSystemPath(resolvedURL, kCFURLPOSIXPathStyle);
				}
			}
		}
	}
	
	if (resolvedPath==nil) {
		return path;
	}
	
	return resolvedPath;
}


- (void)openAllTheseFiles:(NSArray *)arrayOfFiles
{
	NSString *filename;
	for (filename in arrayOfFiles) {
        [self shouldOpen:[self resolveAliasInPath:filename] withEncoding:0];
    }
}

- (void)shouldOpen:(NSString *)path withEncoding:(NSStringEncoding)chosenEncoding
{
    NSAppleEventDescriptor *appleEventSelectionRangeDescriptor = [[[NSAppleEventManager sharedAppleEventManager] currentAppleEvent] paramDescriptorForKeyword:keyAEPosition];
    NSString *lineString;
    if (appleEventSelectionRangeDescriptor) {
        AppleEventSelectionRange selectionRange;
        [[appleEventSelectionRangeDescriptor data] getBytes:&selectionRange length:sizeof(AppleEventSelectionRange)];
         lineString = [NSString stringWithFormat:@"%d", selectionRange.lineNum+1];
    } else {
        lineString = [NSString stringWithFormat:@"%d", 1];
    }
    NSMutableString *command = [NSMutableString stringWithString:@"/usr/local/bin/rider \""];
    [command appendString:path];
    [command appendString:@"\":"];
    [command appendString:lineString];
    system([command UTF8String]);
}
@end



# Unicycle #

This is the source for a simple helper application to use Project Rider IDE From Jetbrains on a Mac with full Unity integration. It intercepts the open files / goto line numbers from within Unity and passes them to the rider command line tool.

Setup:

* Download the binary [here](https://bitbucket.org/quickfingerz/unicycle/downloads)  and put in /Applications
* Set Unity to use Unicycle as the external script editor.
* Add SyncRiderProject.cs to Editor folder in your project assets. [Click here for the script](https://bitbucket.org/quickfingerz/unicycle/src/eeba32cc2292bfcd163e6bfef24b975803b9c0b4/SyncRiderProject.cs?at=master)
* In your Unity Project Click Assets -> Sync Rider Project. Rider will launch and any action within Unity will now open Rider to correct file / line numbers.

To contact, visit www.quickfingers.net or @quickfingerz on Twitter.